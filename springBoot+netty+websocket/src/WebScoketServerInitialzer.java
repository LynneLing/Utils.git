package com.ling.tools.login.netty;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.codec.http.websocketx.WebSocketServerProtocolHandler;
import io.netty.handler.stream.ChunkedWriteHandler;

/**
 * @Author LiangLing
 * @Description TODO
 * @Date 2019/12/16
 **/
public class WebScoketServerInitialzer  extends ChannelInitializer<SocketChannel> {

    @Override
    protected void initChannel(SocketChannel socketChannel) throws Exception {
        ChannelPipeline pipeline = socketChannel.pipeline();

        //http解码器
        pipeline.addLast(new HttpServerCodec());
        //对写大数据流的支持
        pipeline.addLast(new ChunkedWriteHandler());
        //设置单次请求的文件的大小
        pipeline.addLast(new HttpObjectAggregator(1024*1024*10));
        //webscoket 服务器处理的协议，用于指定给客户端连接访问的路由 :/ws
        pipeline.addLast(new WebSocketServerProtocolHandler("/ws"));
        //自定义handler(作用类似controller,客户端和服务器端之间发消息都在这个自定义handler里面)
        pipeline.addLast("handler",new WebSocketHandler());



        }
}
