package com.ling.tools.login.netty;


import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import io.netty.handler.stream.ChunkedWriteHandler;
import io.netty.handler.timeout.IdleStateHandler;
import io.netty.util.concurrent.DefaultEventExecutorGroup;
import io.netty.util.concurrent.EventExecutorGroup;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

@Component
public class WebSocketServer {

    private static EventExecutorGroup group = new DefaultEventExecutorGroup(1024);

    public void run(String... args) throws Exception {

        //创建EventLoopGroup两个线程
        EventLoopGroup bossGroup = null;
        EventLoopGroup workerGroup = null;
        try {
            bossGroup = new NioEventLoopGroup();
            workerGroup = new NioEventLoopGroup();
            //服务对象
            ServerBootstrap serverBootstrap = new ServerBootstrap();
            //构建工程线程池
            serverBootstrap.group(bossGroup, workerGroup)
                    //Nio长连接
                    .channel(NioServerSocketChannel.class)
                    .childOption(ChannelOption.SO_KEEPALIVE, true)
                    .option(ChannelOption.SO_BACKLOG, 1024)
                    .handler(new LoggingHandler(LogLevel.INFO))
                    //初始化 过滤 解码
                    .childHandler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        protected void initChannel(SocketChannel ch) throws Exception {
                            ChannelPipeline pipeline = ch.pipeline();
                            //配置http解码组件
                            pipeline.addLast("http-codec", new HttpServerCodec());
                            pipeline.addLast("ping", new IdleStateHandler(2, 1, 3, TimeUnit.MINUTES));
                            //把多个消息转换为一个单一 Http请求或响应
                            pipeline.addLast("aggregator", new HttpObjectAggregator(65536));
                            //文件传输
                            pipeline.addLast("http-chunked", new ChunkedWriteHandler());
                            //逻辑  连接 服务ing 异常 断开
                            pipeline.addLast(group, "handler", new WebSocketHandler());
                        }
                    });
            //绑定监听端口号
            ChannelFuture channelFuture = serverBootstrap.bind(8082).sync();
            //关闭
            channelFuture.channel().closeFuture().sync();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (bossGroup != null) {
                bossGroup.shutdownGracefully();
            }

            if (workerGroup != null) {
                workerGroup.shutdownGracefully();
            }
        }
    }
}
