package com.ling.tools.login.netty;

import java.util.concurrent.ThreadFactory;


/**
 * 自定义线程工厂
 *
 * @Author LiangLing
 * @Description TODO
 * @Date 2019/12/16
 **/
public class WorkThreadFactoryBuilder implements ThreadFactory {

    private int counter;
    private String name;

    public WorkThreadFactoryBuilder(String name) {
        this.name = name;
    }

    @Override
    public Thread newThread(Runnable r) {
        Thread thread = new Thread(r, name + "-Thread_" + counter);
        counter++;
        return thread;
    }
}
