package com.ling.tools.login.netty;

import java.util.concurrent.*;

/**
 * 自定义线程工厂
 *
 * @Author LiangLing
 * @Description TODO
 * @Date 2019/12/16
 **/
public class WorkThreadPool {

    private ExecutorService executorService = null;

    public WorkThreadPool(){

        ThreadFactory customThreadFactoryBuilder = new WorkThreadFactoryBuilder("localThread");

        executorService = new ThreadPoolExecutor(5, 20,
                0L, TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<>(1024), customThreadFactoryBuilder, new ThreadPoolExecutor.AbortPolicy());
    }

    public ExecutorService getPool(){
        return executorService;
    }
}
